import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class TableUnitTest {
    
    public TableUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception{
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception{
    }
    
    @Before
    public void setUp() throws Exception{
    }
    
    @After
    public void tearDown() throws Exception{
    }
    
    @Test
    public void testCheckRow1(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCheckRow2(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCheckRow3(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCheckCol1(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testCheckCol2(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());
    }
    public void testCheckCol3(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCheckX1(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCheckX2(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testSwitchTurn() {
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        if(table.getCurrentPlayer().getName()== 'O'){
            table.switchTurn();
            assertEquals('X',table.getCurrentPlayer().getName());
        }else{
            table.switchTurn();
            assertEquals('O',table.getCurrentPlayer().getName());
        } 
    }
    public void testSetPoint(){
        Player o = new Player('O',0,0,0);
        Player x = new Player('X',0,0,0);
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(1, table.getCurrentPlayer().getWin());
    }
}
